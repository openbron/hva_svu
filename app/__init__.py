# Copyright 2013 Joey Toppin, Karl Chow
#
# This file is part of svu.
# 
# svu is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# svu is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with svu.  If not, see <http://www.gnu.org/licenses/>.

import os, datetime
from flask import Flask, session, request, render_template, redirect,\
                  url_for, send_from_directory, current_app, g
from flask.ext.sqlalchemy import SQLAlchemy
import flask_sijax
import cgi

# from dir.file import class:
from validate.validate import Validate
from validate.validate import ALLOWED_EXTENSIONS
from validate.validate import MAGIC_VALIDATION

# preamble:
validator = Validate()

app = Flask(__name__)
basedir = os.path.abspath(os.path.dirname(__file__))

app.config["SQLALCHEMY_DATABASE_URI"] = \
"sqlite:///" + os.path.join(basedir, "secure.db")

# load secret key from KEY file, key is closed after it's set
# to log the SECRET_KEY, add the log below in a context i.e. a route function.
# current_app.logger.debug(app.config["SECRET_KEY"])
key = open(basedir+"/KEY", "r")
app.config["SECRET_KEY"] = key.readline()
key.close()

UPLOAD_DIR = basedir+"/uploads"
app.config["UPLOAD_FOLDER"] = UPLOAD_DIR

# flask configuration
path = os.path.join(".", os.path.dirname(__file__), "static/vendor/sijax")
app.config["SIJAX_STATIC_PATH"] = path
app.config["SIJAX_JSON_URI"] = "/static/vendor/sijax/json2.js"

flask_sijax.Sijax(app)

app.debug = True
db = SQLAlchemy(app)

# import database models, e.g. Users
from data.models import *


#
# this route features:
#
# secure file upload showcase.
# html encoding.
# secure reflected and stored showcase.
#
@flask_sijax.route(app, "/")
def secure():

    if request.method == "POST":
        if "file" in request.form.values():
            file = request.files["file"]
            if file and validator.allowed_file(file.filename):

                # secures the name of an uploaded file
                # for more info see werkzeug's secure_filename
                filename = validator.secure(file.filename)

                # python magic identification and validation
                # file.stream is the input stream of the uploaded file
                # usu. a temporarily open file
                # also see validate.py
                # also see http://werkzeug.pocoo.org/docs/datastructures/
                #current_app.logger.debug((file.stream).getvalue()) 
                filetype = validator.identify(file)
                if filetype == 0:
                    current_app.logger.debug("upload succes")

                    # save file on disk, file is also closed
                    file.save(os.path.join(app.config["UPLOAD_FOLDER"],
                                           filename))
                else:
                   error = errmsg(filetype) 
                   return render_template("index.html", errmsg=error, \
                                              allowed=list(ALLOWED_EXTENSIONS))

                return redirect(url_for("uploaded_file", filename=filename))

            else:
                filetype = validator.identify(file)
                error = errmsg(filetype)
                return render_template("index.html", errmsg=error, \
                                              allowed=list(ALLOWED_EXTENSIONS))
        if "text" in request.form.values():
            return render_template("index.html",\
            hax="<script>alert('If you see this in an alert box i.e a popup\
 window, you assumed backend output would be safe to view for the user. Quickly\
 now! Enable autoescaping once again! If it is plain text however, you have\
 done well!')</script>")


    # the cgi module unfortunately does not escape apostrophes
    # also see:i https://wiki.python.org/moin/EscapingHtml 
    escape_apos = {
      "'": "&apos;"   
    }

    def secure_setquery(obj_response, input):

        # return encoded html with the "basic" cgi module
        # note: by default cgi only escapes &, < and >
        # optionally quotes are also escaped for a more robust implementation
        ret = cgi.escape(input["search"], quote=True)
        ret = "".join(escape_apos.get(c, c) for c in ret)

        # uncomment the line below to see what ret looks like before it's sent.
        current_app.logger.debug(ret)

        # or view your browser console to see how ret is returned to the browser
        obj_response.call("set", [ret])

    def secure_subscribe(obj_response, input):

       # input validation, same routine as secure_setquery

       # username
       u = cgi.escape(input["username"], quote=True)
       u = "".join(escape_apos.get(c, c) for c in u)

       # description
       d = cgi.escape(input["description"], quote=True)
       d = "".join(escape_apos.get(c, c) for c in d)

       # create a new user
       user = Users(u, d)

       if user:

         # database transaction
         db.session.add(user)
         db.session.commit()

         # return autoescaped description (jinja2) below #

         # return a succes message and the user details
         obj_response.call("setmsg", [0, user.username, user.description])

       else:

         # return a failure message
         obj_response.call("setmsg", [1])

    g.sijax.register_callback("s_query", secure_setquery)
    g.sijax.register_callback("s_subscribe", secure_subscribe)

    if g.sijax.is_sijax_request:
        return g.sijax.process_request()

    # render template:
    return render_template("index.html", allowed=list(ALLOWED_EXTENSIONS))

@app.route("/uploads/<filename>")
def uploaded_file(filename):
    return send_from_directory(app.config["UPLOAD_FOLDER"], filename)

def errmsg(ferr):
    current_app.logger.debug("upload failure")
    return "Could not upload the file or filetype version ", ferr


#
# insecure reflected and stored showcase
#
@flask_sijax.route(app, "/insecure.html")
def insecure():

    def setquery(obj_response, input):

        # call function "set" in the browser
        # the search term i.e. user input is returned
        obj_response.call("set", [input["search"]])

    def subscribe(obj_response, input):

       # create a new user
       user = Users(input["username"], input["description"])

       if user:

         # database transaction
         db.session.add(user)
         db.session.commit()

         # return a succes message and the user details
         obj_response.call("setmsg", [0, user.username, user.description])

       else:

         # return a failure message
         obj_response.call("setmsg", [1])

    g.sijax.register_callback("query", setquery)
    g.sijax.register_callback("subscribe", subscribe)

    if g.sijax.is_sijax_request:
        return g.sijax.process_request()

    return render_template("insecure.html")

if __name__ == "__main__":
    app.run()
