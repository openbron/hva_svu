# Copyright 2013 Joey Toppin, Karl Chow
#
# This file is part of svu.
# 
# svu is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# svu is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with svu.  If not, see <http://www.gnu.org/licenses/>.

from app import db
from flask.ext.sqlalchemy import SQLAlchemy

class Users(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True)
    description = db.Column(db.Text)

    def __init__(self, username, description):
        self.username = username
        self.description = description

    def __repr__(self):
        return "<User %r>" % self.username
