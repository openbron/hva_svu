# Copyright 2013 Joey Toppin, Karl Chow
#
# This file is part of svu.
# 
# svu is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# svu is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with svu.  If not, see <http://www.gnu.org/licenses/>.

import magic
from werkzeug import secure_filename
from flask import current_app

# rather strict sets of allowed values:
ALLOWED_EXTENSIONS = set(["txt", "pdf", "png"])
MAGIC_VALIDATION = set(["ASCII text", 
                        "PDF document, version 1.2",
                        "PDF document, version 1.3"])
class Validate: 

    def __init__(self):
        self.name = "validator"

    def identify(self, file):
        # file.read() is equivalent to file.stream.read()
        id = magic.from_buffer(file.read())

        if id in MAGIC_VALIDATION:
            print "valid file: ", id
            # reset file position to the beginning, i.e. offset 0
            file.seek(0)
            return 0
        else:
            # return failure:
            print "invalid file: ", id
            file.seek(0)
            return id

    def allowed_file(self, filename):
        return "." in filename and \
            filename.rsplit(".", 1)[1] in ALLOWED_EXTENSIONS

    def secure(self, filename):
        # prevent malicious paths
        # e.g.: ../../home/username/.bashrc
        # becomes "home_user_.bashrc"
        return secure_filename(filename)
